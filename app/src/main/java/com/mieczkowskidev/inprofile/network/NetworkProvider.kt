package com.mieczkowskidev.inprofile.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
class NetworkProvider {

    companion object {
        const val BASE_URL = "https://api.instagram.com/v1/"
    }

    fun getRetrofitClient(): NetworkService = provideRetrofit(provideBaseUrl(),
            provideGson(), provideOkHttpClient(provideHttpLoggingInterceptor()))
            .create(NetworkService::class.java)

    private fun provideRetrofit(baseUrl: String, gson: Gson, okHttpClient: OkHttpClient):
            Retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

    private fun provideBaseUrl(): String = BASE_URL

    private fun provideGson(): Gson = GsonBuilder().create()

    private fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

    private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }

}