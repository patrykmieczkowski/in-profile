package com.mieczkowskidev.inprofile.network

import com.mieczkowskidev.inprofile.media.model.RecentMediaData
import com.mieczkowskidev.inprofile.profile.model.UserData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
interface NetworkService {

    @GET("users/self")
    fun getUser(@Query("access_token") accessToken: String): Single<UserData>

    @GET("users/self/media/recent")
    fun getMedia(@Query("access_token") accessToken: String): Single<RecentMediaData>
}