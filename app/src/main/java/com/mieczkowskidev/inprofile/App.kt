package com.mieczkowskidev.inprofile

import android.app.Application
import com.mieczkowskidev.inprofile.utils.DataPrefs

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
val prefs: DataPrefs by lazy {
    App.prefs!!
}

class App : Application() {

    companion object {
        var prefs: DataPrefs? = null
    }

    override fun onCreate() {
        super.onCreate()

        initPrefs()
    }

    private fun initPrefs() {
        prefs = DataPrefs(applicationContext)
    }
}