package com.mieczkowskidev.inprofile.media

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.mieczkowskidev.inprofile.R
import com.mieczkowskidev.inprofile.media.model.RecentMedia
import com.mieczkowskidev.inprofile.utils.inflate
import kotlinx.android.synthetic.main.media_item.view.*

/**
 * Created by Patryk Mieczkowski on 13.07.2018
 */
class MediaAdapter : RecyclerView.Adapter<MediaAdapter.MediaViewHolder>() {

    private var recentMediaList: List<RecentMedia> = ArrayList()

    fun refreshMediaAdapterList(recentMediaList: List<RecentMedia>) {
        this.recentMediaList = recentMediaList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaViewHolder {
        val itemView = parent.inflate(R.layout.media_item)
        return MediaViewHolder(itemView)
    }

    override fun getItemCount(): Int = recentMediaList.size

    override fun onBindViewHolder(holder: MediaViewHolder, position: Int) {
        val recentMedia = recentMediaList[position]
        holder.bindViews(recentMedia)
    }

    class MediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var view: View = itemView
        private var recentMedia: RecentMedia? = null

        fun bindViews(recentMedia: RecentMedia) {
            this.recentMedia = recentMedia

            // IMAGE
            Glide.with(view.context.applicationContext)
                    .load(recentMedia.images.low_resolution.url)
                    .into(view.media_item_image)

        }
    }
}