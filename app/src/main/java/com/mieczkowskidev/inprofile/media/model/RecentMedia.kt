package com.mieczkowskidev.inprofile.media.model

/**
 * Created by Patryk Mieczkowski on 13.07.2018
 */
data class RecentMedia(var images: RecentMediaImage = RecentMediaImage(), var type: String = "")

data class RecentMediaData(var data: List<RecentMedia> = ArrayList())

data class RecentMediaImage(var low_resolution: RecentMediaImageDetails = RecentMediaImageDetails(),
                            var thumbnail: RecentMediaImageDetails = RecentMediaImageDetails())

data class RecentMediaImageDetails(var url: String = "")