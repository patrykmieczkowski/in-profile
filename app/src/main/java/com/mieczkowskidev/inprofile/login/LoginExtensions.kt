package com.mieczkowskidev.inprofile.login

import android.net.Uri

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
private const val clientId = "f46d0279a04645cfaba6a55345c505ca"
private const val redirectUri = "https://testowo.mytest95.com/login/callback"
const val accessToken = "access_token"

fun instaUri() = Uri.Builder()
        .scheme("https")
        .authority("api.instagram.com")
        .appendPath("oauth")
        .appendPath("authorize")
        .appendQueryParameter("client_id", clientId)
        .appendQueryParameter("redirect_uri", redirectUri)
        .appendQueryParameter("response_type", "token")
        .build().toString()