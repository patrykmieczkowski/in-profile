package com.mieczkowskidev.inprofile.login

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import com.mieczkowskidev.inprofile.R
import kotlinx.android.synthetic.main.login_dialog.view.*

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
class LoginDialog : DialogFragment() {

    companion object {

        fun newInstance(): LoginDialog = LoginDialog()
    }

    lateinit var loginAction: LoginAction

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val myView = inflater.inflate(R.layout.login_dialog, container, false)

        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        myView.login_web_view.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                onPageFinishedAction(myView, url)

            }
        }

        myView.login_web_view.settings.javaScriptEnabled = true
        myView.login_web_view.loadUrl(instaUri())

        return myView
    }

    private fun onPageFinishedAction(myView: View, url: String?) {
        hideProgressBar(myView)

        val token = url?.substringAfter("$accessToken=", "")

        if (token != null && token.isNotEmpty()) {
            loginAction.loginTokenReceived(token)
            closeDialog()
        }
    }


    private fun hideProgressBar(myView: View) {
        myView.login_progress_bar.visibility = View.GONE
    }

    private fun closeDialog() {
        dialog.dismiss()
    }

    interface LoginAction {

        fun loginTokenReceived(token: String)
    }
}