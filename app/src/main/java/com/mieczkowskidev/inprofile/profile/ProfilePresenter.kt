package com.mieczkowskidev.inprofile.profile

import android.util.Log
import com.mieczkowskidev.inprofile.media.model.RecentMedia
import com.mieczkowskidev.inprofile.network.NetworkProvider
import com.mieczkowskidev.inprofile.prefs
import com.mieczkowskidev.inprofile.profile.model.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
class ProfilePresenter : ProfileContract.Presenter {

    companion object {
        val TAG: String = ProfilePresenter::class.java.simpleName
    }

    private var view: ProfileContract.View? = null
    private var networkProvider: NetworkProvider = NetworkProvider()
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun attach(view: ProfileContract.View) {
        this.view = view

        downloadUserProfile()
        downloadRecentMedia()
    }

    private fun downloadUserProfile() {

        val userDisposable = networkProvider.getRetrofitClient()
                .getUser(prefs.accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { getUserHandleSuccess(it.data) },
                        { getUserHandleError(it) }
                )

        compositeDisposable.add(userDisposable)
    }

    private fun downloadRecentMedia() {

        val mediaDisposable = networkProvider.getRetrofitClient()
                .getMedia(prefs.accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { getRecentMediaHandleSuccess(it.data) },
                        { getRecentMediaHandleError(it) }
                )

        compositeDisposable.add(mediaDisposable)
    }

    private fun getRecentMediaHandleSuccess(recentMediaList: List<RecentMedia>) {
        if (recentMediaList.isNotEmpty()) {
            view?.hideMediaProgress()
            view?.refreshRecycler(recentMediaList)
        } else {
            view?.hideMediaProgress()
            view?.showEmptyMedia()
        }
    }

    private fun getUserHandleSuccess(user: User) {
        view?.showUserProfile(user)
    }

    private fun getRecentMediaHandleError(throwable: Throwable) {
        Log.e(TAG, "throwable $throwable")
        view?.hideMediaProgress()
        view?.showEmptyMedia()
    }

    private fun getUserHandleError(throwable: Throwable) {
        Log.e(TAG, "throwable $throwable")
    }

    override fun detach() {
        compositeDisposable.clear()
        this.view = null
    }

    override fun logOut() {
        prefs.accessToken = ""
        view?.startMainActivity()
    }

}