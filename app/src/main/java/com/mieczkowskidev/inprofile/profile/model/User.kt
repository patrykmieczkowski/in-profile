package com.mieczkowskidev.inprofile.profile.model

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
data class User(var username: String = "", var profile_picture: String = "",
                var full_name: String = "", var counts: UserCounts = UserCounts())

data class UserData(var data: User = User())

data class UserCounts(var media: Long = 0, var follows: Long = 0, var followed_by: Long = 0)