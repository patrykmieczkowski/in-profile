package com.mieczkowskidev.inprofile.profile

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.mieczkowskidev.inprofile.MainActivity
import com.mieczkowskidev.inprofile.R
import com.mieczkowskidev.inprofile.media.MediaAdapter
import com.mieczkowskidev.inprofile.media.model.RecentMedia
import com.mieczkowskidev.inprofile.profile.model.User
import com.mieczkowskidev.inprofile.profile.model.UserCounts
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.user_stats.*

class ProfileActivity : AppCompatActivity(), ProfileContract.View {

    private var presenter: ProfileContract.Presenter = ProfilePresenter()

    private var mediaAdapter = MediaAdapter()
    private var layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        initPresenter()
        initRecycler()
        initListeners()
    }

    private fun initPresenter() {
        presenter.attach(this)
    }

    private fun initRecycler() {
        profile_recycler.layoutManager = layoutManager
        profile_recycler.adapter = mediaAdapter
    }

    override fun refreshRecycler(recentMediaList: List<RecentMedia>) {
        mediaAdapter.refreshMediaAdapterList(recentMediaList)
    }

    private fun initListeners() {
        profile_logout.setOnClickListener { presenter.logOut() }
    }

    override fun showUserProfile(user: User) {

        setProfileImage(user.profile_picture)
        setProfileName(user.username, user.full_name)
        setProfileCounts(user.counts)
    }

    private fun setProfileCounts(counts: UserCounts) {
        user_comment_desc.text = "${counts.media}"
        user_follow_desc.text = "${counts.followed_by}"
        user_likes_desc.text = "${counts.follows}"
    }

    private fun setProfileImage(profile_picture: String) {
        Glide.with(this)
                .load(profile_picture)
                .into(profile_image)
    }

    private fun setProfileName(username: String, full_name: String) {
        profile_username.text = username
        profile_full_name.text = full_name
    }

    override fun onDestroy() {
        presenter.detach()
        super.onDestroy()
    }

    override fun startMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun hideMediaProgress() {
        profile_media_progress.visibility = View.GONE
    }

    override fun showEmptyMedia() {
        profile_media_text.visibility = View.VISIBLE
    }

}
