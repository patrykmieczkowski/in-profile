package com.mieczkowskidev.inprofile.profile

import com.mieczkowskidev.inprofile.media.model.RecentMedia
import com.mieczkowskidev.inprofile.profile.model.User

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
interface ProfileContract {

    interface View {
        fun showUserProfile(user: User)
        fun startMainActivity()
        fun refreshRecycler(recentMediaList: List<RecentMedia>)
        fun hideMediaProgress()
        fun showEmptyMedia()
    }

    interface Presenter {

        fun attach(view: View)
        fun detach()
        fun logOut()
    }
}