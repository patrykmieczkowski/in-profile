package com.mieczkowskidev.inprofile

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mieczkowskidev.inprofile.login.LoginDialog
import com.mieczkowskidev.inprofile.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), LoginDialog.LoginAction {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        login_button.setOnClickListener { showLoginDialog() }

        if (prefs.isUserLogged) {
            startProfileActivity()
        }
    }

    private fun showLoginDialog() {

        val loginDialog = LoginDialog.newInstance()

        loginDialog.show(supportFragmentManager, LoginDialog::class.java.simpleName)
        loginDialog.loginAction = this
    }

    override fun loginTokenReceived(token: String) {
        prefs.accessToken = token
        startProfileActivity()
    }

    private fun startProfileActivity() {
        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
        finish()
    }

}
