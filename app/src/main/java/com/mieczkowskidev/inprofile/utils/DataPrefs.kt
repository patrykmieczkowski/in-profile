package com.mieczkowskidev.inprofile.utils

import android.content.Context
import android.content.SharedPreferences
import com.mieczkowskidev.inprofile.BuildConfig

/**
 * Created by Patryk Mieczkowski on 12.07.2018
 */
class DataPrefs(context: Context) {

    private val PREFS_NAME = "${BuildConfig.APPLICATION_ID}.dataprefs"
    private val ACCESS_TOKEN_KEY = "access_token_key"
    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)

    var accessToken: String
        get() = prefs.getString(ACCESS_TOKEN_KEY, "")
        set(value) = prefs.edit().putString(ACCESS_TOKEN_KEY, value).apply()

    var isUserLogged: Boolean = false
        get() = accessToken.isNotEmpty()

}